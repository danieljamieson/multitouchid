Multi-Touch Identification
============================


A few known issues:

- Online identification is in place but needs to be tested thoroughly
- The magnitude of the touch and IMU signals still don't match perfectly
- There's a massive delay on the IMU signal