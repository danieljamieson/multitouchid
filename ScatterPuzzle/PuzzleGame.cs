﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using Microsoft.Surface.Presentation.Controls;
using SSC = Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Input;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    class PuzzleGame
    {
        #region Public Members

        public int index = 0;

        #endregion

        #region Private Members

        Window1 parent;

        // The PuzzleManager.
        private readonly PuzzleManager puzzleManager = new PuzzleManager();

        // The puzzle image.
        private VisualBrush puzzleBrush;

        // Random number generator to create random animations.
        private readonly Random random = new Random();

        // The dimensions to use when loading a puzzle.
        private int maxDifficulty = 5;
        private int minDifficulty = 2;

        private int rowCount = 3;
        private int colCount = 3;

        // Animation events could step on each other's toes, need something to lock on
        private bool joinInProgress;

        // to keep track of when a puzzle is 
        private int joins = 0;
        /***/

        // Brushes to use to stroke puzzle pieces
        private ArrayList brushes = new ArrayList();

        #endregion

        #region Initialisation

        public PuzzleGame()
        {
            parent = Window1.window;
            LoadPuzzleList();
            LoadBrushes();
        }

        /// <summary>
        /// Starts the puzzle game
        /// </summary>
        public void Start()
        {
            parent.main.ColumnDefinitions.Clear();
            Visual newPuzzle = (Viewbox)parent.viewboxes[index];
            LoadVisualAsPuzzle(newPuzzle, Direction.Right);
        }

        /// <summary>
        /// Load all the images/movies in the sample images/movies directories into the puzzle list.
        /// </summary>
        private void LoadPuzzleList()
        {
            /***/
            // Load photo puzzles
            parent.loadImages();
        }

        /// <summary>
        /// Loads six brushes for use by the LoadVisualAsPuzzle function in order to stroke the edges of the puzzle pieces
        /// </summary>
        private void LoadBrushes()
        {
            // Each brush is used for a different player
            brushes.Add(Brushes.Red);
            brushes.Add(Brushes.Blue);
            brushes.Add(Brushes.Yellow);
            brushes.Add(Brushes.Green);
            brushes.Add(Brushes.Purple);
            brushes.Add(Brushes.Pink);
        }

        /// <summary>
        /// Add an image or a movie into the puzzle list
        /// </summary>
        /// <param name="img"></param>
        private void AddElementToPuzzleList(UIElement img)
        {
            // Wrap the item in a Viewbox to constrain its size.
            Viewbox b = new Viewbox { Width = 200, Child = img };
        }

        #endregion

        #region StartPuzzle and Change Difficulty

        //---------------------------------------------------------//
        /// <summary>
        /// Creates puzzle pieces from a visual, and adds them into the ScatterView.
        /// </summary>
        /// <param name="visual"></param>
        void LoadVisualAsPuzzle(Visual visual, Direction fromDirection)
        {
            // The more columns/rows, the less each piece needs to overlap
            float rowOverlap = PuzzleManager.Overlap / rowCount;
            float colOverlap = PuzzleManager.Overlap / colCount;

            puzzleBrush = new VisualBrush(visual);

            // Tell the puzzle manager to load a puzzle with the specified dimensions
            puzzleManager.LoadPuzzle(colCount, rowCount);

            // a colour for each piece is chosen using this index value, which is incremented to shuffle through each player in the game 
            int brush_index = 0;

            for (int row = 0; row < rowCount; row++)
            {
                for (int column = 0; column < colCount; column++)
                {
                    // Calculate the size of the rectangle that will be used to create a viewbox into the puzzle image.
                    // The size is specified as a percentage of the total image size.
                    float boxLeft = (float)column / (float)colCount;
                    float boxTop = (float)row / (float)rowCount;
                    float boxWidth = 1f / colCount;
                    float boxHeight = 1f / rowCount;

                    // Items in column 0 don't have any male puzzle parts on their side, all others do
                    if (column != 0)
                    {
                        boxLeft -= colOverlap;
                        boxWidth += colOverlap;
                    }

                    // Items in row 0 don't have any male puzzle parts on their top, all others do
                    if (row != 0)
                    {
                        boxTop -= rowOverlap;
                        boxHeight += rowOverlap;
                    }

                    // Make a visual brush based on the rectangle that was just calculated.
                    VisualBrush itemBrush = new VisualBrush(visual);
                    itemBrush.Viewbox = new Rect(boxLeft, boxTop, boxWidth, boxHeight);
                    itemBrush.ViewboxUnits = BrushMappingMode.RelativeToBoundingBox;

                    // Get the shape of the piece
                    Geometry shape = GetPieceGeometry(column, row);

                    /***/
                    double scale = 1.0; // 0.5 + random.NextDouble() * (1.0 - 0.5);
                    /***/

                    //shape = new RectangleGeometry(new Rect(5, 5, 200, 200));

                    // Put the brush into a puzzle piece
                    PuzzlePiece piece = new PuzzlePiece(column + (colCount * row), shape, itemBrush);

                    // Set the sensor ID of this piece for identification purposes
                    piece.sensorID = brush_index;

                    // increment the brush index so that we use a different colour for the next piece
                    brush_index++; // make sure we reset the index before we reach a colour for a non-existant player
                    if (brush_index >= Window1.SENSOR_COUNT) brush_index = 0;

                    /***/
                    piece.setScale(scale);
                    /***/

                    // Setup label to take touch count text
                    Label label = new Label();
                    label.Content = "0";
                    label.FontSize = 72.0;
                    label.Foreground = Brushes.Yellow;
                    label.Padding = new Thickness(2.0);
                    label.Visibility = Visibility.Hidden;
                    DropShadowEffect shadow = new DropShadowEffect();
                    shadow.ShadowDepth = 0;
                    label.Effect = shadow;

                    Canvas.SetRight(label, 40); // Alignment- this should be roughly fine
                    Canvas.SetTop(label, 20);

                    // Create stroked edge separately
                    System.Windows.Shapes.Path edge = new System.Windows.Shapes.Path();
                    edge.Data = shape;
                    edge.Stroke = (Brush)brushes[brush_index];
                    edge.StrokeThickness = 3.5;

                    // Add canvas to group them all together
                    Canvas group = new Canvas();
                    group.Children.Add(piece);
                    //group.Children.Add(edge);
                    group.Children.Add(label);

                    // Add the PuzzlePiece to a ScatterViewItem
                    ScatterViewItem item = new ScatterViewItem();
                    item.Content = group;

                    // setup logging
                    item.Uid = ((row * colCount) + column).ToString();

                    // Handle touch events
                    item.PreviewTouchDown += parent.item_TouchDown;
                    item.PreviewTouchUp += parent.item_TouchUp;

                    // Register a name for this item with this class for animation purposes
                    string piecename = "ScatterViewItem" + (column + (colCount * row));

                    piece.animationName = piecename;

                    if (parent.FindName(piecename) != null)
                    {
                        parent.UnregisterName(piecename);
                    }
                    parent.RegisterName(piece.animationName, item);

                    /***/
                    //item.Width = Math.Round(piece.ClipShape.Bounds.Width * scale, 0);
                    //item.Height = Math.Round(piece.ClipShape.Bounds.Height * scale, 0);

                    //item.SizeChanged += new SizeChangedEventHandler(item_SizeChanged);

                    /***/
                    // Set the initial size of the item and prevent it from being resized
                    //item.Width = Math.Round(piece.ClipShape.Bounds.Width, 0);
                    //item.Height = Math.Round(piece.ClipShape.Bounds.Height, 0);
                    // we want to be able to scale the item
                    item.CanScale = false;

                    // Set the item's data context so it can use the piece's shape
                    Binding binding = new Binding();
                    binding.Source = group;
                    item.SetBinding(ScatterViewItem.DataContextProperty, binding);

                    // Animate the item into view
                    AddPiece(item, fromDirection);
                }
            }
        }

        /// <summary>
        /// Changes the difficulty of the game by increasing the number of jigsaw pieces the photo is 
        /// split up into
        /// </summary>
        public void changeDifficulty()
        {
            rowCount++;
            colCount++;

            if (rowCount > maxDifficulty)
                rowCount = colCount = minDifficulty;
        }

        /// <summary>
        /// Getter for rowCount so we can access it as the index of the difficulty array
        /// </summary>
        /// <returns></returns>
        public int getRowCount()
        {
            return rowCount;
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Returns a geometry that represents the shape of the piece, determined by the piece's row and column.
        /// </summary>
        /// <param name="row">The piece's row</param>
        /// <param name="column">The piece's column</param>
        private Geometry GetPieceGeometry(int column, int row)
        {
            System.Windows.Shapes.Path path;
            if (row == 0)
            {
                if (column == 0)
                {
                    path = (System.Windows.Shapes.Path)parent.Resources["TopLeftCorner"];
                }
                else if (column == colCount - 1)
                {
                    path = (System.Windows.Shapes.Path)parent.Resources["TopRightCorner"];
                }
                else
                {
                    path = (System.Windows.Shapes.Path)parent.Resources["TopEdge"];
                }
            }
            else if (row == rowCount - 1)
            {
                if (column == 0)
                {
                    path = (System.Windows.Shapes.Path)parent.Resources["BottomLeftCorner"];
                }
                else if (column == colCount - 1)
                {
                    path = (System.Windows.Shapes.Path)parent.Resources["BottomRightCorner"];
                }
                else
                {
                    path = (System.Windows.Shapes.Path)parent.Resources["BottomEdge"];
                }
            }
            else if (column == 0)
            {
                path = (System.Windows.Shapes.Path)parent.Resources["LeftEdge"];
            }
            else if (column == colCount - 1)
            {
                path = (System.Windows.Shapes.Path)parent.Resources["RightEdge"];
            }
            else
            {
                path = (System.Windows.Shapes.Path)parent.Resources["Center"];
            }

            return path.Data.GetFlattenedPathGeometry();
        }

        //---------------------------------------------------------//
        /// <summary>
        /// returns the application to its initial state where all movies are 
        /// playing and muted, and no pieces are on the board. 
        /// </summary>
        void Reset()
        {
            // If a puzzle is currently loaded and it is a video, mute it
            if (puzzleBrush != null)
            {
                MediaElement media = puzzleBrush.Visual as MediaElement;
                if (media != null)
                {
                    media.IsMuted = true;
                }
            }

            // Remove all puzzle pieces
            RemoveAllPieces();
        }

        #endregion

        #region Puzzle Assembly

        //---------------------------------------------------------//
        /// <summary>
        /// Search the puzzle area for a ScatterViewItem that can be joined to the piece that was 
        /// passed as an argument. If a match is found, join the pieces.
        /// </summary>
        /// <param name="item">The piece that could potentially be joined to another piece.</param>
        public void StartJoinIfPossible(ScatterViewItem item)
        {
            // Compare this piece against all the other pieces in the puzzle
            foreach (ScatterViewItem potentialItem in parent.scatter.Items)
            {
                // Don't even bother trying to join a piece to itself
                if (potentialItem == item)
                {
                    continue;
                }

                // See if the pieces are eligible to join
                if (puzzleManager.CanItemsJoin(item, potentialItem))
                {
                    // The pieces are eligible, join them
                    JoinItems(potentialItem, item);

                    // Only join one set of pieces per manipulation
                    break;
                }
            }
        }

        #endregion

        #region Animations

        //---------------------------------------------------------//
        /// <summary>
        /// Animates all current pieces off the side of the screen.
        /// </summary>
        private void RemoveAllPieces()
        {
            if (parent.scatter.Items.Count == 0)
            {
                //selectionEnabled = true;
                return;
            }

            // Use a for loop here instead of a foreach so the variable used in the animation complete 
            // callback is not modified between the time the callback is hooked up and the time it is called.
            for (int i = 0; i < parent.scatter.Items.Count; i++)
            {
                ScatterViewItem item = (ScatterViewItem)parent.scatter.Items[i];
                PointAnimation remove = ((PointAnimation)parent.Resources["RemovePiece"]).Clone();

                // Can't animate if center isn't set yet, which would happen if a piece has not yet been manipulated
                if (double.IsNaN(item.Center.X))
                {
                    item.Center = item.ActualCenter;
                }

                // Set up a callback that passes the ScatterViewItem that will be needed when the animation completes
                remove.Completed += delegate(object sender, EventArgs e)
                {
                    OnRemoveAnimationCompleted(item);
                };

                // Start the animation
                item.BeginAnimation(ScatterViewItem.CenterProperty, remove, HandoffBehavior.SnapshotAndReplace);
            }
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Called when a remove animation has been completed.
        /// </summary>
        /// <param name="item">The item that was animated</param>
        private void OnRemoveAnimationCompleted(ScatterViewItem item)
        {
            parent.scatter.Items.Remove(item);
            //selectionEnabled = true;
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Adds a ScatterViewItem to the ScatterView, and animates it 
        /// on from the specified side of the screen (Left or Right).
        /// </summary>
        /// <param name="item">The item to add.</param>
        /// <param name="fromDirection">The direction from which puzzle pieces enter.</param>
        private void AddPiece(ScatterViewItem item, Direction fromDirection)
        {
            // Add the piece to the ScatterView at the correct location
            Debug.Assert(fromDirection == Direction.Right || fromDirection == Direction.Left);
            item.Center = fromDirection == Direction.Right ? new Point(1024, 384) : new Point(-200, 384);
            item.Orientation = random.Next(0, 360);
            parent.scatter.Items.Add(item);

            // Load the animation
            Storyboard add = ((Storyboard)parent.Resources["AddPiece"]).Clone();

            foreach (AnimationTimeline animation in add.Children)
            {
                // If this is a double animation, it animates the item's orientation
                DoubleAnimation orientation = animation as DoubleAnimation;
                if (orientation != null)
                {
                    // Spin the orientation a little.
                    orientation.To = item.Orientation + random.Next(-135, 135);
                }

                // If this is a point animation, then it animates the item's center
                PointAnimation center = animation as PointAnimation;
                if (center != null)
                {
                    // Get a random point to animate the item to
                    center.To = new Point(random.Next(0, 773), random.Next(0, 768));
                }
            }

            // Set up a callback that passes the ScatterViewItem that will be needed when the animation completes
            add.Completed += delegate(object sender, EventArgs e)
            {
                OnAddAnimationCompleted(item);
            };

            // Start the animation
            item.BeginStoryboard(add, HandoffBehavior.SnapshotAndReplace);
        }

        /// <summary>
        /// Animates a ScatterViewItem to the designated point
        /// </summary>
        /// <param name="item">The ScatterViewItem to animate</param>
        /// <param name="target">The point in the window to move the ScatterViewItem to</param>
        private void ResetPiece(ScatterViewItem item, Point targetCenter, double targetRotation)
        {
            PointAnimation move = new PointAnimation();
            move.Duration = TimeSpan.FromSeconds(0.2);
            move.From = item.Center;
            move.To = targetCenter;
            move.FillBehavior = FillBehavior.Stop;

            DoubleAnimation rotate = new DoubleAnimation();
            rotate.Duration = TimeSpan.FromSeconds(0.2);
            rotate.From = item.Orientation;
            rotate.To = targetRotation;
            rotate.FillBehavior = FillBehavior.Stop;

            PuzzlePiece pp = PuzzleManager.getPuzzlePieceFromScatterViewItem(item);
            Storyboard.SetTargetName(move, pp.animationName);
            Storyboard.SetTargetProperty(
                move, new PropertyPath(ScatterViewItem.CenterProperty));
            Storyboard.SetTargetName(rotate, pp.animationName);
            Storyboard.SetTargetProperty(
                rotate, new PropertyPath(ScatterViewItem.OrientationProperty));

            Storyboard reset = new Storyboard();
            reset.Children.Add(move);
            reset.Children.Add(rotate);

            // When the animation is completed then we want to move the item to it's target and cancel the
            // animation so that the piece can move again
            reset.Completed += delegate(object sender, EventArgs e)
            {
                item.Center = targetCenter;
                item.Orientation = targetRotation;
                item.CancelManipulation();
            };

            reset.Begin(parent);
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Called when an add animation is completed.
        /// </summary>
        /// <param name="item">The item that was just animated.</param>
        private static void OnAddAnimationCompleted(ScatterViewItem item)
        {
            // When the animation completes, the animation will no longer be the determining factor for the layout
            // of the item, it will revert to the value with the nextmost precedence. In this case, it will be the 
            // values assigned to the item's center and orientation. Set those values to their current values (while
            // the item is still under animation) so that when the animation completes, the item won't appear to jump.
            item.Center = item.ActualCenter;
            item.Orientation = item.ActualOrientation;
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Animates two ScatterViewItems together and then merges the content of both items into one ScatterViewItem.
        /// </summary>
        /// <param name="pieceRemaining">The piece that will remain after the join.</param>
        /// <param name="pieceBeingRemoved">The piece that will be removed as a result of the join.</param>
        private void JoinItems(ScatterViewItem pieceRemaining, ScatterViewItem pieceBeingRemoved)
        {
            // Simultaneous joins on the same pieces (as in the case where two matching pieces are dropped next 
            // to each other at the same time) eventually remove both matching pieces. Make sure only one join
            // happens at one time.
            if (!joinInProgress)
            {
                joinInProgress = true;

                Storyboard join = ((Storyboard)parent.Resources["JoinPiece"]).Clone();

                foreach (AnimationTimeline animation in join.Children)
                {
                    // If this is a double animation, then it animates the piece's orientation
                    DoubleAnimation orientation = animation as DoubleAnimation;
                    if (orientation != null)
                    {
                        orientation.To = pieceRemaining.ActualOrientation;
                        orientation.From = pieceBeingRemoved.ActualOrientation;

                        // If two pieces are close in orientation, but seperated by the 0/360 line (i.e. 3 and 357) then don't spin the piece all the way around
                        if (Math.Abs(pieceRemaining.ActualOrientation - pieceBeingRemoved.ActualOrientation) > 180)
                        {
                            orientation.To += orientation.From > orientation.To ? 360 : -360;
                        }
                    }

                    // If this is a point animation, then it animates the piece's center
                    PointAnimation center = animation as PointAnimation;
                    if (center != null)
                    {
                        center.To = puzzleManager.CalculateJoinAnimationDestination(pieceRemaining, pieceBeingRemoved);
                    }

                    // Can't animate values that are set to NaN
                    if (double.IsNaN(pieceBeingRemoved.Orientation))
                    {
                        pieceBeingRemoved.Orientation = pieceBeingRemoved.ActualOrientation;
                    }

                    // Set up a callback that passes the ScatterViewItems that will be needed when the animation completes
                    join.Completed += delegate(object sender, EventArgs e)
                    {
                        OnJoinAnimationCompleted(pieceBeingRemoved, pieceRemaining);
                    };

                    pieceBeingRemoved.BeginStoryboard(join);
                }
            }
        }

        //---------------------------------------------------------//
        /// <summary>
        /// Called when an join animation is completed.
        /// </summary>
        /// <param name="itemBeingRemoved">The item that should be removed after the join.</param>
        /// <param name="itemRemaining">The item that will remain after the join.</param>
        private void OnJoinAnimationCompleted(ScatterViewItem itemBeingRemoved, ScatterViewItem itemRemaining)
        {
            if (parent.scatter.Items.Contains(itemBeingRemoved) && parent.scatter.Items.Contains(itemRemaining))
            {
                // Get the content for the joined piece
                PuzzlePiece pieceBeingRemoved = PuzzleManager.getPuzzlePieceFromScatterViewItem(itemBeingRemoved);
                PuzzlePiece pieceRemaining = PuzzleManager.getPuzzlePieceFromScatterViewItem(itemRemaining);

                PuzzlePiece joinedPiece = puzzleManager.JoinPieces(pieceBeingRemoved, pieceRemaining);

                // When size changes, center also must be adjusted so the piece doesn't jump
                Vector centerAdjustment = puzzleManager.CalculateJoinCenterAdjustment(pieceRemaining, joinedPiece);
                centerAdjustment = PuzzleManager.Rotate(centerAdjustment, itemRemaining.ActualOrientation);

                // Replace the item's content with the new group
                Canvas canvas = (Canvas)itemRemaining.Content;

                // Get the label out of the canvas to re-add
                Label label = parent.getLabelFromCanvas(canvas);

                // Create stroked edge separately
                System.Windows.Shapes.Path edge = new System.Windows.Shapes.Path();
                //      edge.Data = joinedPiece.ClipShape;
                edge.Stroke = (Brush)brushes[pieceRemaining.sensorID];
                edge.StrokeThickness = 3.5;

                // Get label and re-add it to the canvas after clearing
                canvas.Children.Clear();
                canvas.Children.Add(joinedPiece);
                //canvas.Children.Add(edge);
                canvas.Children.Add(label);

                itemRemaining.Content = canvas;


                /***/
                double scale = pieceBeingRemoved.getScale();
                joinedPiece.setScale(scale);

                // it seems the problem presists with the bare minimum, changing the width of the item is intact but not it's image
                /***/
                // Resize the item to the size of the peice
                //        itemRemaining.Width = Math.Round(joinedPiece.ClipShape.Bounds.Width * scale, 0);
                //       itemRemaining.Height = Math.Round(joinedPiece.ClipShape.Bounds.Height * scale, 0);

                // Adjust the center
                itemRemaining.Center = itemRemaining.ActualCenter + centerAdjustment;

                // Bind the item to the new piece
                Binding binding = new Binding();
                binding.Source = joinedPiece;
                itemRemaining.SetBinding(ScatterViewItem.DataContextProperty, binding);
                itemRemaining.SetRelativeZIndex(RelativeScatterViewZIndex.AboveInactiveItems);

                // Remove the old item from the ScatterView
                parent.scatter.Items.Remove(itemBeingRemoved);

                /***/
                joins = joins + 1;
                /***/

                // Set this to false before StartJoinIfPossible. If there is another join, JoinPieces will set joinInProgress to true,
                // and then it will immediately be set back when StartJoinIfPossible returns.
                joinInProgress = false;

                // See if there are any other pieces elligible to join to the newly joined piece
                StartJoinIfPossible(itemRemaining);
            }
            else
            {
                // Still want to set this to false, even if no join was performed
                joinInProgress = false;
            }

            /***/
            if (puzzleCompleted())
            {
                // reset to remove the completed puzzle
                Reset();
                Visual newPuzzle = (Viewbox)parent.viewboxes[index];
                LoadVisualAsPuzzle(newPuzzle, Direction.Left);
            }
            /***/
        }

        #endregion

        #region Completed Puzzle

        /// <summary>
        /// Called when a join is completed to check whether the puzzle is completed
        /// </summary>
        /// <returns>Has the puzzle completed or not</returns>
        private bool puzzleCompleted()
        {
            // The number of joins to complete the puzzle is
            // the number of pieces minus one.
            if (joins == rowCount * colCount - 1)
            {
                if (index == parent.viewboxes.Count - 1)
                {
                    index = 0;
                }
                else
                {
                    index = index + 1;
                }
                joins = 0;

                return true;
            }
            return false;
        }



        #endregion
    }
}