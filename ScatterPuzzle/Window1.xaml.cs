using System;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
/***/
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using SMARTTableSDK.Core;
using System.Windows.Shapes;
/***/

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    /// <summary>
    /// Main Window
    /// </summary>
    public partial class Window1 : Window
    {
        #region Public Members

        /***/
        // to hold the list of images that will be used throughout the study
        public ArrayList images = new ArrayList();
        public ArrayList viewboxes = new ArrayList();

        public static Window1 window;
        public static TouchManager touchManager;

        public static int SENSOR_COUNT;
        public static long START_TIME;

        public string[] comPorts;
        public int touchID = 0;  // increments with each touch down on a scatterviewitem
        public static double dpiX, dpiY; // X and Y dpi of the screen

        #endregion

        #region Private Members

        private Identification identifier;

        private PuzzleGame game;

        // Paths where puzzles should be loaded from.
        //private readonly string videoPuzzlesPath;
        private readonly string photoPuzzlesPath;

        // Difficulty identifiers
        private string[] difficulty_names = new string[6] { "", "", "Easy", "Medium", "Hard", "Hardest" };

        #endregion

        #region Initalization

        //---------------------------------------------------------//
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Window1()
        {
            InitializeComponent();
            window = this;

            // query the registry to find out where sample photos and videos are stored.
            const string systemFoldersKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\explorer\Shell Folders";
            photoPuzzlesPath = (string)Microsoft.Win32.Registry.GetValue(systemFoldersKey, "CommonPictures", null) + @"\Sample Pictures";

            game = new PuzzleGame();
        }

        /// <summary>
        /// Called when the window is loaded
        /// </summary>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // for slate
            dpiX = 190.73; dpiY = 190.73;

            // for samsung sur40
            //dpiX = 55.07; dpiY = 55.07;

#if SUPPORT_SMART_TABLE
            // Support multi-touch on the SMART tables
            TableSDK.Instance.Register(this);
            dpiX = 45; dpiY = 45;
#endif
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            // Close log files
        }

        public void loadImages()
        {
            // Load photo puzzles
            foreach (string file in Directory.GetFiles(photoPuzzlesPath, "*.jpg"))
            {
                // Load the photo
                Image img = new Image();
                img.Source = new BitmapImage(new Uri(file));

                /***/
                // add the image to the list of images
                images.Add(img);

                Viewbox b = new Viewbox { Width = 200, Child = img };
                viewboxes.Add(b);
            }
        }

        #endregion

        #region ScatterViewItem Event Handlers

        /// <summary>
        /// Called when a touch occurs on a ScatterViewItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void item_TouchDown(object sender, TouchEventArgs e)
        {
            ScatterViewItem item = sender as ScatterViewItem;
            // The puzzle piece class is stored in the 'Content' property of the ScatterViewItem
            PuzzlePiece pp = PuzzleManager.getPuzzlePieceFromScatterViewItem(item);

            // Increment touch counter for label
            touchID++;
            totaltoucheslabel.Content = touchID;
            Label label = getLabelFromCanvas((Canvas)item.Content);
            label.Content = touchID;
            label.Visibility = Visibility.Visible;

            // apply greyscale effect
            if (!pp.beingTouched())
            {
                pp.Effect = new Effects.GreyscaleEffect();
            }

            pp.lastPosition = item.Center;
            pp.lastOrientation = item.Orientation;
            TouchManager.addTouchSignal(e.TouchDevice, touchID);
        }

        /// <summary>
        /// Called when the ScatterViewItem stops being touched
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void item_TouchUp(object sender, TouchEventArgs e)
        {
            ScatterViewItem item = sender as ScatterViewItem;
            PuzzlePiece pp = PuzzleManager.getPuzzlePieceFromScatterViewItem(item);

            Label label = getLabelFromCanvas((Canvas)item.Content);
            label.Visibility = Visibility.Hidden;

            identifier.offlineIdentification(e.TouchDevice);

            TouchManager.removeTouchSignal(e.TouchDevice);

            if (pp.beingTouched())
            {
                pp.touchesDown--;

                if (!pp.beingTouched())
                    pp.Effect = null;

                // Validate input
                if (item == null)
                    return;

                game.StartJoinIfPossible(item);
            }
        }

        /// <summary>
        /// Called when an item is scaled by the user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void item_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ScatterViewItem viewItem = (ScatterViewItem)sender;
            Canvas content = (Canvas)viewItem.Content;

            PuzzlePiece piece = PuzzleManager.getPuzzlePieceFromScatterViewItem(viewItem);

            double scale = viewItem.Width / piece.originalWidth;

            piece.ClipShape.Transform = new ScaleTransform(scale, scale);
            piece.setScale(scale);
            piece.Height = Math.Round(piece.ClipShape.Bounds.Height, 0);
            //piece.originalHeight = piece.Height;
            piece.Width = Math.Round(piece.ClipShape.Bounds.Width, 0);
        }

        /***/
        #endregion

        #region Utility functions

        /// <summary>
        /// Utility method for getting a label out of its canvas
        /// </summary>
        /// <param name="canvas"></param>
        /// <returns></returns>
        public Label getLabelFromCanvas(Canvas canvas)
        {
            foreach (UIElement child in canvas.Children)
            {
                if (child is Label)
                {
                    return (Label)child;
                }
            }
            return null;
        }

        private void ShowImage()
        {
            scatter.Visibility = Visibility.Hidden;
            puzzleImage.Visibility = Visibility.Visible;
            currentImage.Source = ((Image)images[game.index]).Source;
        }

        public string getPhotoPuzzlesPath()
        {
            return photoPuzzlesPath;
        }

        #endregion

        #region Button Callbacks

        /// <summary>
        /// Called when the user clicks the connect button in order to connect to the IMU's over
        /// bluetooth and start the puzzle game
        /// </summary>
        private void connectBtn_Click(object sender, RoutedEventArgs e)
        {
            comPorts = new string[2];
            comPorts[0] = "com6"; comPorts[1] = "com20";
            if (comPorts != null)
            {
                // Hide the old menu
                menuPanel.Visibility = Visibility.Hidden;
                menuPanel.IsEnabled = false;
                
                // Set up identification with IMU signals
                SENSOR_COUNT = comPorts.Length;
                identifier = new Identification(SENSOR_COUNT);
                IMUSignal[] signals = new IMUSignal[SENSOR_COUNT];
                for (int i = 0; i < comPorts.Length; i++)
                {
                    IMUSignal newSensorSignal = new IMUSignal(new CWASensor(comPorts[i]));
                    signals[i] = newSensorSignal;
                    identifier.addIMUsignal(i, newSensorSignal);
                }

                touchManager = new TouchManager(signals);
                START_TIME = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                game.Start();
            }
        }

        /// <summary>
        /// Shows the settings option box so that the user can select which COM ports to connect to
        /// the sensors over
        /// </summary>
        private void settingsBtn_Click(object sender, RoutedEventArgs e)
        {
            Settings stsWindow = new Settings(this);
            stsWindow.ShowDialog();
        }

        /// <summary>
        /// Allows the user to switch between different difficulty modes aka. the number of pieces
        /// on the game board
        /// </summary>
        private void difficultyBtn_Click(object sender, RoutedEventArgs e)
        {
            game.changeDifficulty();
            difficultyBtn.Content = difficulty_names[game.getRowCount()];
        }
        
        #endregion

    }
}