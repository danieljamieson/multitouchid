﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    /// <summary>
    /// Interaction logic for SignalComparison.xaml
    /// </summary>
    public partial class SignalComparison : Window
    {
        public SignalComparison(int idx)
        {
            InitializeComponent();
            Title = "Signal Comparison" + idx;
        }

        /// <summary>
        /// Draws a graph of two magnitude signals on the canvas
        /// </summary>
        /// <param name="y1"></param>
        /// <param name="y2"></param>
        public void drawGraphs(double[] y1, double[] y2, double difference)
        {
            if (y1 == null || y2 == null)
                return;
            differenceLabel.Content = "Difference: " + difference;
            canvas1.Children.Clear();
            Polyline pl = createPath(Brushes.Black, 2, canvas1);
            Polyline p2 = createPath(Brushes.Red, 2, canvas1);

            int totalY = (int)canvas1.RenderSize.Height;
            int totalX = (int)canvas1.RenderSize.Width;
            int xInterval = totalX / y1.Length;
            Point pt;
            double min = double.MaxValue;
            double max = double.MinValue;
            for (int x = 0, i = 0; i < y1.Length && i < y2.Length; i++, x += xInterval)
            {
                if (y1[i] > max)
                    max = y1[i];
                else if (y1[i] < min)
                    min = y1[i];

                if (y2[i] > max)
                    max = y2[i];
                else if (y2[i] < min)
                    min = y2[i];
            }
            double yScale = (50) / (max - min);
            for (int x = 0, i = 0; i < y1.Length && i < y2.Length; i++, x += xInterval)
            {
                pt = new Point(x, 300 - (y1[i] * yScale));
                pl.Points.Add(pt);

                pt = new Point(x, 300 - (y2[i] * yScale));
                p2.Points.Add(pt);
            }
        }

        /// <summary>
        /// Creates a polyline path on the specified canvas
        /// </summary>
        /// <param name="brsh"></param>
        /// <param name="thickness"></param>
        /// <param name="parentCanvas"></param>
        /// <returns></returns>
        private Polyline createPath(Brush brsh, int thickness, Canvas parentCanvas)
        {
            Polyline pl = new Polyline();
            parentCanvas.Children.Add(pl);
            pl.Stretch = Stretch.None;
            pl.StrokeEndLineCap = PenLineCap.Round;
            pl.StrokeLineJoin = PenLineJoin.Round;
            Canvas.SetLeft(pl, 0);
            Canvas.SetTop(pl, 0);
            pl.Stroke = brsh;
            pl.StrokeThickness = thickness;
            return pl;
        }
    }
}
