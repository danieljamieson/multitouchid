﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        Window1 parent;
        public Settings(Window1 _parent)
        {
            InitializeComponent();
            parent = _parent;
        }

        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            String coms = comPortsTextbox.Text;
            string[] ports = coms.Split(',');

            parent.comPorts = ports;

            this.Close();
        }
    }
}
