﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.ComponentModel;
using System.Windows;
using System.Text.RegularExpressions;
using System.Threading;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class CWASensor
    {
        #region Public Members

        public volatile bool INITIALISED = false;

        #endregion

        #region Private Members

        private int resets = 0;
        private string COM_PORT = "COM24";
        private SerialPort port;
        private readonly BackgroundWorker portWorker = new BackgroundWorker(); // Performs connection work asynchronously
        
        // For storing the data
        private List<byte> data_buffer = new List<byte>();

        // For calculating quaternions from acceleration and gyro
        private MadgwickAHRS ahrs;

        // For sampling via pattern matching the stream of data
        private Regex rx;
        private const string SAMPLE_PATTERN = @"(?<ax>-?\d+),(?<ay>-?\d+),(?<az>-?\d+),(?<gx>-?\d+),(?<gy>-?\d+),(?<gz>-?\d+)";
        private const float A_DIV = 4096.0f;    // Divide accelerometer data to get correct units
        private const float G_MUL = 0.07f;      // Multiply gyroscope data to get correct units

        #endregion

        /// <summary>
        /// Default constructor which attempts to connect to the sensor
        /// </summary>
        /// <param name="port"></param>
        public CWASensor(string port)
        {
            ahrs    = new MadgwickAHRS();
            rx = new Regex(SAMPLE_PATTERN);
            COM_PORT = port;
            InitializeBackgroundWorkers();
        }

        /// <summary>
        /// Returns the serial port object
        /// </summary>
        public SerialPort getPort()
        {
            return port;
        }

        // HACK: Sort out nicely
        public DateTime DeviceTime { get; protected set; }

        public void UpdateDeviceTime()
        {
            DeviceTime += TimeSpan.FromMilliseconds(IMUSignal.SLEEP);
        }

        public void ResetDeviceTime()
        {
            resets++;
            DeviceTime = DateTime.Now;
        }

        /// <summary>
        /// Initialise event handlers for receiving data from Bluetooth device
        /// </summary>
        private void InitializeBackgroundWorkers()
        {
            this.portWorker.DoWork += new DoWorkEventHandler(portWorker_DoWork);
            this.portWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(portWorker_RunWorkerCompleted);
            this.portWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Establishes serial port connection to Bluetooth device
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void portWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (port == null || !port.IsOpen)
            {
                int success = 1;
                do
                {
                    // Attempt connection
                    try
                    {
                        Console.WriteLine("Sending command sequence to " + COM_PORT);
                        port = new SerialPort(COM_PORT, 9600, Parity.None, 8, StopBits.One);
                        port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                        port.Open();
                        success = 1;
                    }
                    catch (Exception ex)
                    {
                        success = 0;
                        Console.WriteLine("PORT OPEN Exception: " + ex.ToString());
                    }
                    if (success == 0) Thread.Sleep(500);
                } while (success == 0);

                // Now send commands
                try
                {
                    port.WriteLine("device\r\n");
                    port.WriteLine("mode=2\r\n");
                    port.WriteLine("rate x 1 40\r\n");
                    port.WriteLine("stream=1\r\n");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Exception: " + ex.ToString());
                }
            }
        }

        /// <summary>
        /// Receives data from the sensor and places it into the data buffer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            byte[] temp = new byte[port.BytesToRead];
            port.Read(temp, 0, temp.Length);

            lock (data_buffer) 
            {
                data_buffer.AddRange(temp);
            }
        }

        /// <summary>
        /// Fired once serial connection is established; should signal any exceptions
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event arguments</param>
        protected void portWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("Exception: " + e.Error.ToString());
            }
        }

        /// <summary>
        /// Converts degrees to radians
        /// </summary>
        /// <param name="degrees">Angle in degrees</param>
        static float deg2rad(float degrees)
        {
            return (float)(Math.PI / 180) * degrees;
        }

        /// <summary>
        /// Checks the data buffer for samples
        /// </summary>
        /// <returns></returns>
        public IMUData PollPort()
        {
            string line;

            if (!port.IsOpen) return null;

            lock (data_buffer)
            {
                int idx;
                if ((idx = data_buffer.IndexOf((int)'\n')) < 0)
                {
                    line = null;
                }
                else
                {
                    byte[] temp = data_buffer.GetRange(0, idx + 1).ToArray();
                    line = (new ASCIIEncoding()).GetString(temp);
                    data_buffer.RemoveRange(0, idx + 1);
                }
            }

            if (line == null) { return null; }

            IMUData data = null;

            bool success = false;
            Match m = rx.Match(line);
            if (m.Success)
            {
                if (!INITIALISED)
                {
                    INITIALISED = true;
                    Console.WriteLine("IMU initialised at " + COM_PORT);
                }
                try
                {
                    float[] sample = {
                        float.Parse(m.Groups["ax"].Value) / A_DIV,
                        float.Parse(m.Groups["ay"].Value) / A_DIV,
                        float.Parse(m.Groups["az"].Value) / A_DIV,
                        deg2rad(float.Parse(m.Groups["gx"].Value) * G_MUL),
                        deg2rad(float.Parse(m.Groups["gy"].Value) * G_MUL),
                        deg2rad(float.Parse(m.Groups["gz"].Value) * G_MUL)
                    };
                    ahrs.Update(sample[3], sample[4], sample[5], sample[0], sample[1], sample[2]);
                    data = new IMUData(sample, ahrs.Quaternion);
                    //Console.WriteLine("ax={0} ay={1} az={2} gx={3} gy={4} gx={5}", sample[0], sample[1], sample[2], sample[3], sample[4], sample[5]);
                    success = true;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
                catch (OverflowException ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }

            if (!success) { return null; }

            return data;
        }
    }
}