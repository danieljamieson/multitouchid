﻿#pragma checksum "..\..\..\Sources\AnalyzerMainWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "463540A0C3D744EA49E8BBF13BB1CA90"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1008
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Surface.Presentation;
using Microsoft.Surface.Presentation.Controls;
using Microsoft.Surface.Presentation.Controls.Primitives;
using Microsoft.Surface.Presentation.Controls.TouchVisualizations;
using Microsoft.Surface.Presentation.Input;
using Microsoft.Surface.Presentation.Palettes;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Microsoft.Surface.Samples.ScatterPuzzle.Analyzer {
    
    
    /// <summary>
    /// AnalyzerMainWindow
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class AnalyzerMainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OpenTabletoLogBtn;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OpenIMULogBtn;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox tabletopGesturesLB;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox IMUGesturesLB;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button UpdateSameSignalDataBtn;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button compareBtn;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button compareAllBtn;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton noFilterRB;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Filter3RB;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Filter5RB;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock statusLbl;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas tabletopGesturesCanva;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas matchingGesturesCanva;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Surface.Presentation.Controls.ScatterView tabletopGesturesSV;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas tabletopGraphsCanva;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Sources\AnalyzerMainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider ttDelaySlider;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ScatterPuzzle;component/sources/analyzermainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OpenTabletoLogBtn = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.OpenTabletoLogBtn.Click += new System.Windows.RoutedEventHandler(this.OpenTabletoLogBtn_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.OpenIMULogBtn = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.OpenIMULogBtn.Click += new System.Windows.RoutedEventHandler(this.OpenIMULogBtn_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.tabletopGesturesLB = ((System.Windows.Controls.ListBox)(target));
            
            #line 28 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.tabletopGesturesLB.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.tabletopGesturesLB_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.IMUGesturesLB = ((System.Windows.Controls.ListBox)(target));
            return;
            case 5:
            this.UpdateSameSignalDataBtn = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.UpdateSameSignalDataBtn.Click += new System.Windows.RoutedEventHandler(this.UpdateSameSignalDataBtn_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.compareBtn = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.compareBtn.Click += new System.Windows.RoutedEventHandler(this.compareBtn_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.compareAllBtn = ((System.Windows.Controls.Button)(target));
            
            #line 41 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.compareAllBtn.Click += new System.Windows.RoutedEventHandler(this.compareAllBtn_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.noFilterRB = ((System.Windows.Controls.RadioButton)(target));
            
            #line 45 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.noFilterRB.Checked += new System.Windows.RoutedEventHandler(this.noFilterRB_Checked);
            
            #line default
            #line hidden
            return;
            case 9:
            this.Filter3RB = ((System.Windows.Controls.RadioButton)(target));
            
            #line 46 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.Filter3RB.Checked += new System.Windows.RoutedEventHandler(this.Filter3RB_Checked);
            
            #line default
            #line hidden
            return;
            case 10:
            this.Filter5RB = ((System.Windows.Controls.RadioButton)(target));
            
            #line 47 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.Filter5RB.Checked += new System.Windows.RoutedEventHandler(this.Filter5RB_Checked);
            
            #line default
            #line hidden
            return;
            case 11:
            this.statusLbl = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.tabletopGesturesCanva = ((System.Windows.Controls.Canvas)(target));
            return;
            case 13:
            this.matchingGesturesCanva = ((System.Windows.Controls.Canvas)(target));
            return;
            case 14:
            this.tabletopGesturesSV = ((Microsoft.Surface.Presentation.Controls.ScatterView)(target));
            return;
            case 15:
            this.tabletopGraphsCanva = ((System.Windows.Controls.Canvas)(target));
            return;
            case 16:
            this.ttDelaySlider = ((System.Windows.Controls.Slider)(target));
            
            #line 70 "..\..\..\Sources\AnalyzerMainWindow.xaml"
            this.ttDelaySlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.ttDelaySlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

