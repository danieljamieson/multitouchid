
sampler2D implicitInput : register(s0);

float4 main(float2 uv : TEXCOORD) : COLOR
{
    float4 color = tex2D(implicitInput, uv);
    
	float4 average;
    average.rgb = (color.r + (color.g * 3) + (color.b * 4)) / 8;
	average.a = color.a;
	
	return average;
}