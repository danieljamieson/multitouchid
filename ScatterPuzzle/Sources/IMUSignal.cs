﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class IMUSignal
    {
        public static int SLEEP = 25;
        public int MAX_SIZE { get; set; }
        public int SIZE { get; set; }

        CWASensor sensor;
        Queue<IMUData> samples;
        BackgroundWorker thread;

        double[] magnitude;

        /// <summary>
        /// Creates an IMU signal using the CWASensor class to connect over bluetooth
        /// </summary>
        /// <param name="device"></param>
        /// <param name="max_size"></param>
        public IMUSignal(CWASensor device, int max_size = 2000)
        {
            sensor = device;
            MAX_SIZE = max_size;
            samples = new Queue<IMUData>();
            thread = new BackgroundWorker();
            thread.WorkerSupportsCancellation = true;
            thread.DoWork += new DoWorkEventHandler(signal_Updater);
            thread.ProgressChanged += new ProgressChangedEventHandler(signal_Update);
            thread.WorkerReportsProgress = true;
            thread.RunWorkerAsync();
        }

        public double compareSignal(TouchDevice touchDevice, SignalComparison graph)
        {
            double diff = 0.0;
            TouchSignal touchToCompare = TouchManager.retrieveCorrespondingSignal(this, touchDevice);

            if (touchToCompare == null)
                return -1.0;

            double sigma = 0;
            double[] IMUmagnitude = touchToCompare.getIMUmagnitude(samples.ToArray());
            double[] touchMagnitude = touchToCompare.getMagnitude();
            //diff = Identification.magnitudeDynamicTimeWarping(
            diff = Identification.magnitudeDifference(
                    IMUmagnitude,
                    touchMagnitude,
                    out sigma
                    );
            Console.WriteLine("diff: " + diff);
            graph.drawGraphs(IMUmagnitude, touchMagnitude, diff);
            return diff;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void signal_Updater(object sender, DoWorkEventArgs e)
        {
            int currentTime;
            int n = 1;
            int sleepTime;
            while (!thread.CancellationPending)
            {
                currentTime = (int)(System.DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - Window1.START_TIME);
                thread.ReportProgress(currentTime);
                sleepTime = n * SLEEP - currentTime;
                while (sleepTime < 0)
                {
                    n++;
                    sleepTime = n * SLEEP - currentTime;
                };
                Thread.Sleep(sleepTime);
                n++;
            }
        }

        /// <summary>
        /// Updates the IMU signal by polling the sensor and touch signals
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void signal_Update(object sender, ProgressChangedEventArgs e)
        {
            DateTime currentDeviceTime = sensor.DeviceTime;
            IMUData sample = null;
            do
            {
                sample = sensor.PollPort();
                if (sample != null)
                {
                    double timestamp = ((sensor.DeviceTime.Ticks / TimeSpan.TicksPerMillisecond) - Window1.START_TIME) / 1000.0;

                    if (Math.Abs((sensor.DeviceTime - DateTime.Now).TotalMilliseconds) > 50)
                        sensor.ResetDeviceTime();

                    sample.t = timestamp;

                    TouchManager.updateTouchSignals(this, timestamp);

                    samples.Enqueue(sample); SIZE++;
                    if (SIZE > MAX_SIZE) samples.Dequeue();

                    sensor.UpdateDeviceTime();
                }
            } while (sample != null);

            magnitude = IMUData.updateMagnitude(samples.ToArray());

            TouchManager.updateTouchMagnitude(this);
        }
    }
}
