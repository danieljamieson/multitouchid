﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class TouchData
    {
        public double t; // time
        public double magnitude;

        public double x; // x-coordinate
        public double y; // y-coordinate

        double xvelocity;
        double yvelocity;

        double xaccel;
        double yaccel;

        public TouchData(double timestamp, TouchDevice touchDevice)
        {
            t = timestamp;

            System.Windows.Input.TouchPoint initialPoint = touchDevice.GetTouchPoint(Window1.window);
            Point screenSpacePoint = Window1.window.PointToScreen(new Point(initialPoint.Position.X, initialPoint.Position.Y));

            x = (screenSpacePoint.X * 0.0254) / Window1.dpiX;
            y = (screenSpacePoint.Y * 0.0254) / Window1.dpiY;
        }

        /// <summary>
        /// Used to initialise this class with the typical values associated
        /// with the first sample in a touch signal (velocity, accel, mag unknown)
        /// </summary>
        public void firstSample()
        {
            xvelocity = 0;
            yvelocity = 0;
            xaccel = 0;
            yaccel = 0;
            magnitude = 0;
        }

        /// <summary>
        /// Used to initialise this class with the typical values associated
        /// with the second sample in the touch signal (accel, mag unknown)
        /// </summary>
        /// <param name="prevVelocity">Previous sample needed to calculate velocity</param>
        public void secondSample(TouchData previous)
        {
            calculateVelocity(previous);
            xaccel = 0;
            yaccel = 0;
            magnitude = 0;
        }

        /// <summary>
        /// Used to initialise the class with the typical values associated with
        /// any other sample in the touch signal (all values can be known)
        /// </summary>
        /// <param name="prevVelocity">Previous sample needed to calculate velocity</param>
        /// <param name="prevAccel">Previous sample needed to calculate accel</param>
        public void anyOtherSample(TouchData previous)
        {
            calculateVelocity(previous);
            calculateAcceleration(previous);
            calculateMagnitude();
        }

        /// <summary>
        /// Calculates velocity for this sample
        /// </summary>
        /// <param name="prev">The sample from the previous timestep</param>
        public void calculateVelocity(TouchData prev)
        {
            double dt = t - prev.t;

            xvelocity = (x - prev.x) / dt; // dx / dt
            yvelocity = (y - prev.y) / dt; // dy / dt
        }

        /// <summary>
        /// Calculates acceleration for this sample
        /// </summary>
        /// <param name="prev">The sample from the previous timestep</param>
        public void calculateAcceleration(TouchData prev)
        {
            double dt = t - prev.t;

            xaccel = (xvelocity - prev.xvelocity) / dt; // dvx / dt
            yaccel = (yvelocity - prev.yvelocity) / dt; // dyx / dt
        }

        /// <summary>
        /// Calculates magnitude for this sample
        /// </summary>
        /// <param name="prev">The sample from the previous timestep</param>
        public void calculateMagnitude()
        {
            magnitude = Math.Sqrt((Math.Pow(xaccel, 2)) + (Math.Pow(yaccel, 2)));
        }

        public static double[] getMagnitudeArray(TouchData[] data)
        {
            double[] magnitude = new double[data.Length];
            for (int i = 0; i < magnitude.Length; i++)
            {
                magnitude[i] = data[i].magnitude;
            }
            return magnitude;
        }
    }
}
