﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using NDtw;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class TouchManager
    {
        // Store the TouchDevice with the touch counter
        public static Dictionary<TouchDevice, int> touches = new Dictionary<TouchDevice, int>();
        // And the IMUsignals and their respective touch signals
        public static Dictionary<IMUSignal, List<TouchSignal>> IMUtouchInterface = new Dictionary<IMUSignal,List<TouchSignal>>();

        public TouchManager(IMUSignal[] IMUs)
        {
            foreach (IMUSignal IMU in IMUs)
            {
                IMUtouchInterface.Add(IMU, new List<TouchSignal>());
            }
        }
        
        /// <summary>
        /// Allows a new touch interaction to be added
        /// </summary>
        /// <param name="touchDevice">TouchDevice of corresponding interaction</param>
        /// <param name="touchID">The touch counter value at touch down</param>
        public static void addTouchSignal(TouchDevice touchDevice, int touchID)
        {
            touches.Add(touchDevice, touchID);

            foreach (KeyValuePair<IMUSignal, List<TouchSignal>> IMUtouch in IMUtouchInterface)
            {
                IMUtouch.Value.Add(new TouchSignal(touchDevice));
            }
        }

        /// <summary>
        /// Removes a touch signal from the manager
        /// </summary>
        /// <param name="touchDevice"></param>
        public static void removeTouchSignal(TouchDevice touchDevice)
        {
            for (int i = 0; i < IMUtouchInterface.Count; i++)
            {
                List<TouchSignal> touchSignals = IMUtouchInterface.Values.ToArray()[i];
                for (int j = 0; j < touchSignals.Count; j++)
                {
                    if (touchSignals[j].touchDevice == touchDevice)
                    {
                        IMUtouchInterface.Values.ToArray()[i].Remove(touchSignals[j]);
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// Updates each touch signal for the respective IMU signal
        /// </summary>
        /// <param name="IMU">The IMU these touch signals correspond to</param>
        /// <param name="timestamp">The timestamp to give each new touch signal</param>
        public static void updateTouchSignals(IMUSignal IMU, double timestamp)
        {
            foreach (TouchSignal touch in IMUtouchInterface[IMU])
            {
                touch.update(timestamp);
            }
        }

        /// <summary>
        /// Recalculates the magnitude of the touch signals
        /// </summary>
        /// <param name="IMU">Which IMU's touch signals to update</param>
        public static void updateTouchMagnitude(IMUSignal IMU)
        {
            foreach (TouchSignal touch in IMUtouchInterface[IMU])
            {
                touch.updateMagnitude();
            }
        }

        /// <summary>
        /// Retrieves the touch signal corresponding with the touch interaction specified by the touch
        /// device
        /// </summary>
        /// <param name="IMU">The IMU signal which is being compared</param>
        /// <param name="touchDevice">the touch device associated with the touch interaction</param>
        /// <returns></returns>
        public static TouchSignal retrieveCorrespondingSignal(IMUSignal IMU, TouchDevice touchDevice)
        {
            for (int i = 0; i < IMUtouchInterface[IMU].Count; i++)
            {
                if (IMUtouchInterface[IMU][i].touchDevice == touchDevice)
                    return IMUtouchInterface[IMU][i];
            }
            return null;
        }
    }
}