﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class IMUData
    {
        public double t; // time
        public double xymagnitude;

        float[] accel;
        float[] gyro;
        float[] magnetometer;

        float[] eulerAngles;
        float[] quaternion;

        float[] relAccel;
        public float[] linAccel;

        float[] gravity;
        float[] ypr;
        float[] matrixRotation;
        float[] matrixInvRotation;

        public IMUData(float[] sample, float[] quat)
        {
            accel = new float[] { sample[0], sample[1], sample[2] };
            gyro = new float[] { sample[3], sample[4], sample[5] };
            quaternion = quat;

            linAccel = new float[3];
            matrixRotation = new float[16]; 
            matrixInvRotation = new float[16]; 
            relAccel = new float[3]; 

            // Euler
            eulerAngles = QuaternionToEuler(quat);

            // Yaw, pitch, roll angles;  gravity relative to device
            QuaternionToYawPitchRoll(quat, out ypr, out gravity);

            // Calculate relative acceleration for the device after gravity is removed
            float[] point = new float[3];

            MatrixLoadZXYInverseRotation(eulerAngles[2], eulerAngles[1], eulerAngles[0], out matrixInvRotation);

            // (Optional) Recalculate gravity direction:
            point[0] = 0.0f; point[1] = -1.0f; point[2] = 0.0f;
            Point3MultiplyMatrix(point, matrixInvRotation, out gravity);

            // Re-order (YZX)
            relAccel[0] = accel[1] + gravity[0];
            relAccel[1] = accel[2] + gravity[1];
            relAccel[2] = accel[0] + gravity[2];

            // Calculate the linear acceleration in world coordinates
            point = new float[3];

            //QuaternionToMatrix(azimuth->quat, azimuth->matrixRotation);   // TODO: To use this, need to swap axes? (0=1, 1=2, 2=0)
            MatrixLoadZXYRotation(-eulerAngles[2], -eulerAngles[1], -eulerAngles[0], out matrixRotation);
            point[0] = relAccel[0];
            point[1] = relAccel[1];
            point[2] = relAccel[2];

            Point3MultiplyMatrix(point, matrixRotation, out linAccel);

            for (int i = 0; i < linAccel.Length; i++)
            {
                linAccel[i] *= 9.81F;
            }

            /*accel = null;
            gyro = null;
            magnetometer = null;
            eulerAngles = null;
            quaternion = null;
            relAccel = null;
            gravity = null;
            ypr = null;
            matrixRotation = null;
            matrixInvRotation = null;*/

            //xymagnitude = Math.Sqrt( (Math.Pow(linAccel[0],2)) + (Math.Pow(linAccel[2],2)) );
        }

        public static double[] updateMagnitude(IMUData[] data)
        {
            double[] magnitude = new double[data.Length];
            for (int i = 0; i < magnitude.Length; i++)
            {
                magnitude[i] = data[i].xymagnitude;
            }
            return magnitude;
        }

        #region Mathematical Functions
        private static float[] QuaternionToEuler(float[] q)
        {
            float[] angles = new float[3];
            angles[0] = (float)Math.Atan2(2 * q[1] * q[2] - 2 * q[0] * q[3], 2 * q[0] * q[0] + 2 * q[1] * q[1] - 1); // psi
            angles[1] = -(float)Math.Asin(2 * q[1] * q[3] + 2 * q[0] * q[2]); // theta
            angles[2] = (float)Math.Atan2(2 * q[2] * q[3] - 2 * q[0] * q[1], 2 * q[0] * q[0] + 2 * q[3] * q[3] - 1); // phi
            return angles;
        }

        private static void QuaternionToYawPitchRoll(float[] q, out float[] ypr, out float[] gxyz)
        {
            ypr = new float[3];
            gxyz = new float[3];

            float gx, gy, gz; // estimated gravity direction
            float sgyz, sgxz;

            gx = 2 * (q[1] * q[3] - q[0] * q[2]);
            gy = 2 * (q[0] * q[1] + q[2] * q[3]);
            gz = q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3];

            sgyz = (float)Math.Sqrt(gy * gy + gz * gz);
            sgxz = (float)Math.Sqrt(gx * gx + gz * gz);

            ypr[0] = (float)Math.Atan2(2 * q[1] * q[2] - 2 * q[0] * q[3], 2 * q[0] * q[0] + 2 * q[1] * q[1] - 1);
            ypr[1] = (sgyz < 0.05f) ? 0.0f : (float)Math.Atan(gx / sgyz);
            ypr[2] = (sgxz < 0.05f) ? 0.0f : (float)Math.Atan(gy / sgxz);

            gxyz[0] = gx;
            gxyz[1] = gy;
            gxyz[2] = gz;

            // Swap axes to match output
            float t = gxyz[0];
            gxyz[0] = -gxyz[1];
            gxyz[1] = -gxyz[2];
            gxyz[2] = -t;
        }

        private static void MatrixLoadZXYRotation(float phi, float theta, float psi, out float[] matrix)
        {
            // TODO: Speed this up by composing the matrix manually 
            float[] rotX = new float[16], rotY = new float[16], rotZ = new float[16], temp = new float[16];
            MatrixLoadZRotation(out rotZ, phi);    // Z: phi (roll)
            MatrixLoadXRotation(out rotX, theta);  // X: theta (pitch)
            MatrixLoadYRotation(out rotY, psi);    // Y: psi (yaw)
            MatrixMultiply(rotZ, rotX, out temp);
            MatrixMultiply(temp, rotY, out matrix);
        }

        private static void MatrixLoadZXYInverseRotation(float phi, float theta, float psi, out float[] matrix)
        {
            // TODO: Speed this up by composing the matrix manually 
            float[] rotX = new float[16], rotY = new float[16], rotZ = new float[16], temp = new float[16];
            MatrixLoadZRotation(out rotZ, phi);    // Z: phi (roll)
            MatrixLoadXRotation(out rotX, theta);  // X: theta (pitch)
            MatrixLoadYRotation(out rotY, psi);    // Y: psi (yaw)
            MatrixMultiply(rotY, rotX, out temp);
            MatrixMultiply(temp, rotZ, out matrix);
        }

        private static void MatrixLoadXRotation(out float[] dest, float angleRadians)
        {
            dest = new float[16];
            dest[0] = 1.0f; dest[4] = 0.0f; dest[8] = 0.0f; dest[12] = 0.0f;
            dest[1] = 0.0f; dest[5] = (float)Math.Cos(angleRadians); dest[9] = -(float)Math.Sin(angleRadians); dest[13] = 0.0f;
            dest[2] = 0.0f; dest[6] = (float)Math.Sin(angleRadians); dest[10] = (float)Math.Cos(angleRadians); dest[14] = 0.0f;
            dest[3] = 0.0f; dest[7] = 0.0f; dest[11] = 0.0f; dest[15] = 1.0f;
        }

        private static void MatrixLoadYRotation(out float[] dest, float angleRadians)
        {
            dest = new float[16];
            dest[0] = (float)Math.Cos(angleRadians); dest[4] = 0.0f; dest[8] = (float)Math.Sin(angleRadians); dest[12] = 0.0f;
            dest[1] = 0.0f; dest[5] = 1.0f; dest[9] = 0.0f; dest[13] = 0.0f;
            dest[2] = -(float)Math.Sin(angleRadians); dest[6] = 0.0f; dest[10] = (float)Math.Cos(angleRadians); dest[14] = 0.0f;
            dest[3] = 0.0f; dest[7] = 0.0f; dest[11] = 0.0f; dest[15] = 1.0f;
        }

        private static void MatrixLoadZRotation(out float[] dest, float angleRadians)
        {
            dest = new float[16];
            dest[0] = (float)Math.Cos(angleRadians); dest[4] = -(float)Math.Sin(angleRadians); dest[8] = 0.0f; dest[12] = 0.0f;
            dest[1] = (float)Math.Sin(angleRadians); dest[5] = (float)Math.Cos(angleRadians); dest[9] = 0.0f; dest[13] = 0.0f;
            dest[2] = 0.0f; dest[6] = 0.0f; dest[10] = 1.0f; dest[14] = 0.0f;
            dest[3] = 0.0f; dest[7] = 0.0f; dest[11] = 0.0f; dest[15] = 1.0f;
        }

        private static void MatrixMultiply(float[] matrix1, float[] matrix2, out float[] result)
        {
            result = new float[16];
            float a1, b1, c1, d1, e1, f1, g1, h1, i1, j1, k1, l1;
            float a2, b2, c2, d2, e2, f2, g2, h2, i2, j2, k2, l2;

            // gl-arrangement
            a1 = matrix1[0]; b1 = matrix1[4]; c1 = matrix1[8]; d1 = matrix1[12];
            e1 = matrix1[1]; f1 = matrix1[5]; g1 = matrix1[9]; h1 = matrix1[13];
            i1 = matrix1[2]; j1 = matrix1[6]; k1 = matrix1[10]; l1 = matrix1[14];

            a2 = matrix2[0]; b2 = matrix2[4]; c2 = matrix2[8]; d2 = matrix2[12];
            e2 = matrix2[1]; f2 = matrix2[5]; g2 = matrix2[9]; h2 = matrix2[13];
            i2 = matrix2[2]; j2 = matrix2[6]; k2 = matrix2[10]; l2 = matrix2[14];

            result[0] = a1 * a2 + b1 * e2 + c1 * i2;
            result[1] = e1 * a2 + f1 * e2 + g1 * i2;
            result[2] = i1 * a2 + j1 * e2 + k1 * i2;
            result[3] = 0.0f;

            result[4] = a1 * b2 + b1 * f2 + c1 * j2;
            result[5] = e1 * b2 + f1 * f2 + g1 * j2;
            result[6] = i1 * b2 + j1 * f2 + k1 * j2;
            result[7] = 0.0f;

            result[8] = a1 * c2 + b1 * g2 + c1 * k2;
            result[9] = e1 * c2 + f1 * g2 + g1 * k2;
            result[10] = i1 * c2 + j1 * g2 + k1 * k2;
            result[11] = 0.0f;

            result[12] = a1 * d2 + b1 * h2 + c1 * l2 + d1;
            result[13] = e1 * d2 + f1 * h2 + g1 * l2 + h1;
            result[14] = i1 * d2 + j1 * h2 + k1 * l2 + l1;
            result[15] = 1.0f;
        }
        private static void Point3MultiplyMatrix(float[] point, float[] matrix, out float[] result)
        {
            result = new float[3];
            result[0] = matrix[0] * point[0] + matrix[4] * point[1] + matrix[8] * point[2] + matrix[12];
            result[1] = matrix[1] * point[0] + matrix[5] * point[1] + matrix[9] * point[2] + matrix[13];
            result[2] = matrix[2] * point[0] + matrix[6] * point[1] + matrix[10] * point[2] + matrix[14];
        }

        #endregion
    }
}
