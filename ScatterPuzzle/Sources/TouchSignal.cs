﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class TouchSignal
    {
        public static int MAX_SIZE { get; set; }
        public static int SIZE { get; set; }

        public double[] magnitude;

        //public static int PREV_SAMPLE { get { return SIZE - 1; } } // CHECK THESE ARE SET CORRECTLY

        public TouchDevice touchDevice;
        Queue<TouchData> samples;

        public TouchSignal(TouchDevice device, int max_size = 2000)
        {
            touchDevice = device;
            samples = new Queue<TouchData>();
            MAX_SIZE = max_size;
            SIZE = 0; 
        }

        public void update(double timestamp)
        {
            // Create a new sample and then calculate it's necassary values
            TouchData sample = new TouchData(timestamp, touchDevice);
            int PREV_SAMPLE = samples.Count - 1;
            switch (samples.Count)
            {
                case 0: sample.firstSample();
                    break; 
                case 1: sample.secondSample(samples.ToArray()[PREV_SAMPLE]);
                    break;
                default: sample.anyOtherSample(samples.ToArray()[PREV_SAMPLE]);
                    break;
            }

            samples.Enqueue(sample);
            if (samples.Count > MAX_SIZE) samples.Dequeue();
        }

        public void updateMagnitude()
        {
            magnitude = TouchData.getMagnitudeArray(samples.ToArray());
        }

        public double[] getIMUmagnitudeOld(IMUData[] data)
        {
            if (samples.Count > 0)
            {
                List<double> IMUmagnitude = new List<double>();
                double touchStart = samples.ToArray()[0].t;
                double touchEnd = samples.ToArray()[samples.Count - 1].t;

                for (int i = 0; i < data.Count(); i++) 
                {
                    if (data.ToArray()[i].t >= touchStart && data.ToArray()[i].t <= touchEnd)
                        IMUmagnitude.Add(data.ToArray()[i].xymagnitude);
                }
                return IMUmagnitude.ToArray();
            }
            return null;
        }

        public double[] getMagnitude()
        {
            if (samples.Count > 0)
            {
                double[] x = new double[samples.Count];
                double[] y = new double[samples.Count];
                double[] t = new double[samples.Count];

                for (int i = 0; i < samples.Count(); i++)
                {
                    x[i] = samples.ToArray()[i].x;
                    y[i] = samples.ToArray()[i].y;
                    t[i] = samples.ToArray()[i].t;
                }
                double[] xsamples = Filters.MovingAverageFilter(x, 5);
                double[] ysamples = Filters.MovingAverageFilter(y, 5);
                xsamples = Filters.MovingAverageFilter(xsamples, 5);
                ysamples = Filters.MovingAverageFilter(ysamples, 5);
                double RNG = 0;
                double[] xvelocity = Filters.MovingAverageFilter(getDerivatives(xsamples, t, out RNG), 3);
                double[] yvelocity = Filters.MovingAverageFilter(getDerivatives(ysamples, t, out RNG), 3);

                double[] xaccel = Filters.MovingAverageFilter(getDerivatives(xvelocity, t, out RNG), 3);
                double[] yaccel = Filters.MovingAverageFilter(getDerivatives(yvelocity, t, out RNG), 3);

                double[] magnitude = Filters.MovingAverageFilter(calculateMagnitudes(xaccel, yaccel), 5);

                return magnitude;
            }
            return null;
        }

        public double[] getIMUmagnitude(IMUData[] data)
        {
            if (samples.Count > 0)
            {
                double[] IMUx = new double[samples.Count];
                double[] IMUy = new double[samples.Count];
                double touchStart = samples.ToArray()[0].t;

                int j = 0;
                for (int i = 0; i < data.Count(); i++)
                {
                    if (data.ToArray()[i].t >= touchStart)
                    {
                        IMUx[j] = data.ToArray()[i].linAccel[0];
                        IMUy[j] = data.ToArray()[i].linAccel[2];
                        j++;
                        if (j == samples.Count) break;
                    }
                }
                double[] xaccel = Filters.MovingAverageFilter(IMUx.ToArray(), 3);
                double[] yaccel = Filters.MovingAverageFilter(IMUy.ToArray(), 3);

                double[] magnitude = Filters.MovingAverageFilter(calculateMagnitudes(xaccel, yaccel), 5);

                return magnitude;
            }
            return null;
        }

        private double[] calculateMagnitudes(double[] x, double[] y)
        {
            double[] magnitude = new double[x.Length];
            for (int i = 0; i < magnitude.Length; i++)
            {
                magnitude[i] = Math.Sqrt((Math.Pow(x[i], 2)) + (Math.Pow(y[i], 2)));
            }
            return magnitude;
        }

        /// <summary>
        /// Finds the rate of change between each two successive points 
        /// </summary>
        /// <param name="ds">The set of values for each point</param>
        /// <param name="times">The set of times for each point</param>
        /// <param name="valueRange">Outputs the range of calculated values</param>
        /// <returns></returns>
        double[] getDerivatives(double[] ds, double[] times, out double valueRange)
        {
            double min = double.MaxValue, max = double.MinValue;
            double[] vels = new double[ds.Length];
            for (int i = 0; i < ds.Length; i++)
            {
                if (i > 0)
                {
                    if (times[i] == times[i - 1])
                        vels[i] = vels[i - 1];
                    else
                        vels[i] = (ds[i] - ds[i - 1]) / (times[i] - times[i - 1]);
                }
                else
                    vels[i] = 0;
                if (vels[i] > max)
                    max = vels[i];
                if (vels[i] < min)
                    min = vels[i];
            }
            valueRange = max - min;
            return vels;
        }
    }
}
