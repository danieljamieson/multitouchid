﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using NDtw;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public class Identification
    {
        SignalComparison[] comparisonWindow;

        static int NUM_SENSORS { get; set; }
        IMUSignal[] imuSignals;

        /// <summary>
        /// Creates an Identification class which holds a number of IMU signals and makes
        /// comparisons with touch signals to identify which sensor is associated with
        /// which touch interactions
        /// </summary>
        /// <param name="sensor_count"></param>
        public Identification(int sensor_count)
        {
            NUM_SENSORS = sensor_count;
            imuSignals = new IMUSignal[NUM_SENSORS];

            comparisonWindow = new SignalComparison[NUM_SENSORS];
            for (int i = 0; i < comparisonWindow.Length; i++)
            {
                comparisonWindow[i] = new SignalComparison(i);
                comparisonWindow[i].Show();
            }
        }

        /// <summary>
        /// Adds an IMU signal to consider in future identification events
        /// </summary>
        /// <param name="index">The ID of the sensor</param>
        /// <param name="imu">The IMU signal class</param>
        public void addIMUsignal(int index, IMUSignal imu)
        {
            if (index >= 0 && index < NUM_SENSORS)
            {
                imuSignals[index] = imu;
            }
        }

        /// <summary>
        /// Performs an identification on touch up
        /// </summary>
        /// <param name="touchDevice">The touch interaction to identify</param>
        public void offlineIdentification(TouchDevice touchDevice)
        {
            double[] diffValues = new double[NUM_SENSORS];
            double minDiff = double.MaxValue;
            int minDiffIndex = 0;

            for (int i = 0; i < imuSignals.Length; i++)
            {
                diffValues[i] = imuSignals[i].compareSignal(touchDevice, comparisonWindow[i]);
                if (diffValues[i] < minDiff)
                {
                    minDiff = diffValues[i];
                    minDiffIndex = i;
                }
            }

            Console.WriteLine("identified as sensor " + minDiffIndex);
        }

        /// <summary>
        /// Calculates the difference between two signals by calculating the sum of the 
        /// differences of each point
        /// </summary>
        /// <param name="d1">The first signal</param>
        /// <param name="d2">The second signal</param>
        /// <param name="sigma">The standard deviation of the difference array</param>
        /// <returns></returns>
        public static double magnitudeDifference(double[] d1, double[] d2, out double sigma)
        {
            // calculate the difference and find the total difference
            double[] diff = new double[d1.Length];
            double totalDifference = 0;
            for (int i = 0; i < diff.Length; i++)
            {
                diff[i] = Math.Abs(d2[i] - d1[i]);
                totalDifference += diff[i];
            }
            // find the mean
            double mean = totalDifference / diff.Length;

            // find the variance
            double[] squaredMeanDifference = new double[d1.Length];
            double total = 0;
            for (int i = 0; i < squaredMeanDifference.Length; i++)
            {
                squaredMeanDifference[i] = Math.Pow(diff[i] - mean, 2);
                total += squaredMeanDifference[i];
            }
            double variance = total / squaredMeanDifference.Length;

            // find the standard deviation
            sigma = Math.Sqrt(variance);

            return totalDifference;
        }

        /// <summary>
        /// Calculate the difference between two magnitude signals by using dynamic
        /// time warping
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        public static double magnitudeDynamicTimeWarping(double[] d1, double[] d2)
        {
            if (d1.Length > 0 && d2.Length > 0)
            {
                Dtw cost = new Dtw(d1, d2);
                return cost.GetCost();
            }
            else return double.MaxValue;
        }

        /// <summary>
        /// If the calculated differences for each touch and IMU signal pair are all above a certain
        /// threshold then we can make the decision that the IMU signal with the lowest difference
        /// must be the signal associated with the target touch signal
        /// </summary>
        /// <param name="diff">The calculated differences between the total mangitude difference
        /// values</param>
        /// <returns>Are the calculated differences above the threshold or not</returns>
        private bool allDifferencesAboveThreshold(double[] diff)
        {
            double threshold = 50;
            for (int i = 0; i < diff.Length; i++)
            {
                if (diff[i] < threshold)
                    return false;
            }
            return true;
        }
    }
}
