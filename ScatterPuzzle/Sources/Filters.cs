﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsoft.Surface.Samples.ScatterPuzzle
{
    public static class Filters
    {
        public static double[] MovingAverageFilter(double[] data, int nFilterPoints)
        {
            //This needs to be replaced with a proper general nPoints moving average filter
            //that takes into account the special characteristics of the touch surface signal
            //and the possibility of a small number of points.
            if (nFilterPoints == 0)
                return data;
            else if (nFilterPoints == 3)
                return MovingAverageFilter3(data);
            else
                return MovingAverageFilter5(data);
        }

        //This needs to be replaced with a proper general nPoints moving average filter
        //that takes into account the special characteristics of the touch surface signal
        //and the possibility of a small number of points.
        private static double[] MovingAverageFilter3(double[] data)
        {
            if (data.Length <= 1)
                return data;
            double[] fd = new double[data.Length];
            fd[0] = (data[0] + data[1]) / 2;
            int i;
            for (i = 1; i < (data.Length - 1); i++)
            {
                fd[i] = (data[i - 1] + data[i] + data[i + 1]) / 3;
            }
            fd[i] = (data[i - 1] + data[i]) / 2;

            return fd;
        }

        //This needs to be replaced with a proper general nPoints moving average filter
        //that takes into account the special characteristics of the touch surface signal
        //and the possibility of a small number of points.
        private static double[] MovingAverageFilter5(double[] data)
        {
            if (data.Length <= 4)
                return data;
            double[] fd = new double[data.Length];
            fd[0] = (data[0] + data[1] + data[2]) / 3;
            fd[1] = (data[0] + data[1] + data[2] + data[3]) / 4;
            int i;
            for (i = 2; i < (data.Length - 2); i++)
            {
                fd[i] = (data[i - 2] + data[i - 1] + data[i] + data[i + 1] + data[i + 2]) / 5;
            }
            fd[i] = (data[i - 2] + data[i - 1] + data[i] + data[i + 1]) / 4;
            fd[i + 1] = (data[i - 1] + data[i] + data[i + 1]) / 3;

            return fd;
        }
    }
}
